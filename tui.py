topleft = '╭'
bottomleft = '╰'
topright = '╮'
bottomright = '╯'
topbottomline = '─'
leftrightline = '│'

def tui(lines):
	longest = ""
	longestLen = 0
	for i in lines:
		if len(i) > len(longest):
			longest = i
			longestLen = len(i)

	print(str(topleft) + str(topbottomline) * longestLen + str(topright))
	for i in range(len(lines)):
		print(str(leftrightline) + str(lines[i]) + " " * (longestLen - len(lines[i])) + str(leftrightline))
	print(str(bottomleft) + str(topbottomline) * longestLen + str(bottomright))

# tui(["line 1", "line 2"])
# tui("tall text")
