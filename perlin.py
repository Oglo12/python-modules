from perlin_noise import PerlinNoise

def perlin_2d(x, z, seed = 1, octaves = 1, amp = 1, freq = 1):
    noise = PerlinNoise(seed = seed, octaves = octaves)

    y = noise([x / freq, z / freq]) * amp

    return y

def perlin_1d(x, seed = 1, octaves = 1, amp = 1, freq = 1, offsets = (0)):
    y = perlin_2d(x, offsets[0], seed = seed, octaves = octaves, amp = amp, freq = freq)

    return y

def perlin_3d(x, y, z, seed = 1, octaves = 1, amp = 1, freq = 1, offsets = (0, 0, 0, 0, 0, 0)):
    p = perlin_2d(x + offsets[0], y + offsets[1], seed = seed, octaves = octaves, amp = amp, freq = freq) * perlin_2d(z + offsets[2], x + offsets[3], seed = seed, octaves = octaves, amp = amp, freq = freq) * perlin_2d(z + offsets[4], y + offsets[5], seed = seed, octaves = octaves, amp = amp, freq = freq)

    return p
